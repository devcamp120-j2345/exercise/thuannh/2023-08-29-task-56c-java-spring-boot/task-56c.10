package com.devcamp.countryregionapi.model;

import java.util.ArrayList;

public class Country {
    private String countryCode;
    private String countryName;
    private ArrayList<Region> region;

    public Country() {
    }

    public Country(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public Country(String countryCode, String countryName, ArrayList<Region> region) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.region = region;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public ArrayList<Region> getRegion() {
        return region;
    }

    public void setRegion(ArrayList<Region> region) {
        this.region = region;
    }

}
